﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using AForge.Imaging.Filters;

namespace Image.Filters
{
    public class HandContour : Filter
    {
        private Bitmap imgHandContour; 

        public Bitmap ExtractHandContour(Bitmap img)
        {
            // create filter to remove noise (small blobs)
            BlobsFiltering filterB = new BlobsFiltering();
            // configure filter
            //filterB.CoupledSizeFiltering = true;
            filterB.MinWidth =200;
            //filter.MaxWidth = 200;
            filterB.MinHeight = 400;
            // apply the filter            
            this.imgHandContour = filterB.Apply(img);          
            return imgHandContour;
        }

        public Bitmap Invert()
        {
            Invert inv = new Invert();
            this.imgHandContour = inv.Apply(this.imgHandContour);
            return this.imgHandContour;
        }
    }
}
