﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using AForge.Imaging;

namespace Algorithms
{
    public class HandGeometry
    {
        #region Private Members
              
        private Bitmap imgHandContour = null;
        private int iPixelCount = 0;       
        private Point pHandCentroid = new Point();
        List<Point> lstContourPoints = new List<Point>();
        List<Point> lstCurvaturePoints = new List<Point>();
        List<Point> lstRedPixels = new List<Point>();
        List<Point> contourPoints = new List<Point>();
        List<Point> EdgePoints = new List<Point>();
        Dictionary<double, Point> distances = new Dictionary<double, Point>();
        Graphics graphicsControl = null;

        #endregion

        # region Constructor
        public HandGeometry(Bitmap imgHandContour, ref Graphics g)
        {            
            this.imgHandContour = imgHandContour;
            this.graphicsControl = g;
        }

        #endregion

        #region Properties

        public int PixelCount
        {
            get
            {
                return this.iPixelCount;
            }
        }

        public List<Point> ContourPoints
        {
            get
            {
                return this.contourPoints;
            }
        }

        #endregion
        
        #region Public Methods

        public Bitmap ProccessHandGeometry()
        {            
            this.FindCentroid();
            this.CalculateDistancesFromCentroid();
            this.DetectFingerTips();
            this.DetectHandValleys();
            return this.imgHandContour;
        }             

        public void GetContourPixels()
        {
            this.contourPoints.Clear();

            int iLastLine = this.imgHandContour.Height - 1;
            int iLastColumn = this.imgHandContour.Width;

            //get contour pixels
            for (int y = iLastLine - 1; y >= 0; y--)
            {
                for (int x = 0; x < iLastColumn; x++)
                {
                    if (this.imgHandContour.GetPixel(x, y).Name == "ffffffff") //avoid white and red ones
                        continue;

                    contourPoints.Add(new Point(x, y));
                }
            }
        }

        public void CalculateDistancesFromCentroid()
        {
            Point pBase = this.pHandCentroid;

            this.GetContourPixels();

            foreach (Point p in this.contourPoints)
            {
                if (p.Y >= this.pHandCentroid.Y)
                    continue;

                double distance = Geometry.EuclideanDistance(p, pBase);

                if (!Double.IsNaN(distance) && distance > 0)
                {
                    if (!distances.ContainsKey(distance) && distance > 20)
                    {
                        distances.Add(distance, p);                        
                    }
                }
            }
        }

        public void CalculatePixelCount()
        {
            for (int x = 2; x < this.imgHandContour.Width - 2; x++)
            {
                for (int y = 2; y < this.imgHandContour.Height - 2; y++)
                {
                    Color cl = this.imgHandContour.GetPixel(x, y);
                    if (cl.Name != "ffffffff")
                    {
                        this.iPixelCount++;
                        //lstContourPoints.Add(new AForge.IntPoint(x, y));
                        lstContourPoints.Add(new Point(x, y));
                    }
                }
            }
        }

        public Point FindCentroid()
        {
            int xPoints = 0;
            int yPoints = 0;

            this.GetContourPixels();

            foreach (Point p in this.contourPoints)
            {
                xPoints += p.X;
                yPoints += p.Y;
            }

            this.pHandCentroid.X = xPoints / this.contourPoints.Count;
            this.pHandCentroid.Y = yPoints / this.contourPoints.Count;
            return this.pHandCentroid;
        }

        #endregion

        #region Private Methods

        private void DetectFingerTips()
        {
            //var items = from pair in distances orderby pair.Key descending select pair.Value;
            //var farthestDistances = from pair in distances orderby pair.Key descending select pair;
            var ordered = distances.OrderByDescending(c => c.Key);
            bool bFirstPoint = true;
            int previousXRight = 0;
            int previousXLeft = 0;
            int previousYLeft = 0;
           
            foreach (KeyValuePair<double, Point> kvp in ordered)
            {               
                //middle finger
                if (bFirstPoint)
                {
                    this.imgHandContour.SetPixel(kvp.Value.X, kvp.Value.Y, Color.Red); //this line and the bellow one should be on a separated methos to avoid duplicatiom with the other if bellow
                    //g.DrawEllipse(new Pen(Color.Red, 2), kvp.Value.X, kvp.Value.Y, 10, 10);
                    this.graphicsControl.FillEllipse(new SolidBrush(Color.Red), kvp.Value.X, kvp.Value.Y, 10, 10);
                    bFirstPoint = false;
                    previousXRight = kvp.Value.X;
                    previousXLeft = kvp.Value.X;
                    previousYLeft = kvp.Value.Y;
                    EdgePoints.Add(kvp.Value);
                    continue;
                }

                //right scan
                if (previousXRight + 60 < kvp.Value.X)
                {
                    this.imgHandContour.SetPixel(kvp.Value.X, kvp.Value.Y, Color.Red); //this line and the bellow one should be on a separated methos to avoid duplicatiom with the other if bellow
                    //g.DrawEllipse(new Pen(Color.Red, 2), kvp.Value.X, kvp.Value.Y, 10, 10);
                    this.graphicsControl.FillEllipse(new SolidBrush(Color.Red), kvp.Value.X, kvp.Value.Y, 10, 10);
                    previousXRight = kvp.Value.X;
                    EdgePoints.Add(kvp.Value);
                    continue;
                }

                //left scan
                if (previousXLeft - 100 > kvp.Value.X)
                {
                    this.imgHandContour.SetPixel(kvp.Value.X, kvp.Value.Y, Color.Red); //this line and the bellow one should be on a separated methos to avoid duplicatiom with the other if bellow
                    //g.DrawEllipse(new Pen(Color.Red, 2), kvp.Value.X, kvp.Value.Y, 10, 10);
                    this.graphicsControl.FillEllipse(new SolidBrush(Color.Red), kvp.Value.X, kvp.Value.Y, 10, 10);
                    previousXLeft = kvp.Value.X;
                    previousYLeft = kvp.Value.Y;
                    EdgePoints.Add(kvp.Value);
                }
            }
        }

        private void DetectHandValleys()
        {
            //for a more genereic algorithm order the list by X axis values as in (from r in EdgePoints orderby r.X descending select r.X)) 

            double shortestDistance = 10000d;
            KeyValuePair<double, Point> skvp = new KeyValuePair<double, Point>();

            var orderedAsc = distances.OrderBy(c => c.Key);

            foreach (KeyValuePair<double, Point> kvp in orderedAsc)
            {

                //between thumbs and index
                if (kvp.Value.X > EdgePoints[4].X && kvp.Value.X < EdgePoints[1].X && kvp.Value.Y > EdgePoints[4].Y && kvp.Value.Y < this.imgHandContour.Height - 400)
                {

                    if (kvp.Key < shortestDistance)
                    {
                        skvp = kvp;
                        shortestDistance = kvp.Key;
                    }
                }
            }

            this.imgHandContour.SetPixel(skvp.Value.X, skvp.Value.Y, Color.Red); //this line and the bellow one should be on a separated methos to avoid duplicatiom with the other if bellow
            this.graphicsControl.DrawEllipse(new Pen(Color.Red, 2), skvp.Value.X, skvp.Value.Y, 10, 10);

            int yEdge = skvp.Value.Y;
            orderedAsc = distances.OrderBy(c => c.Key);

            Point[] xpointsDistance = (from xkvp in EdgePoints orderby xkvp.X select xkvp).ToArray<Point>();
            List<KeyValuePair<double, Point>> shortestDistances = new List<KeyValuePair<double, Point>>();

            int leftEdge = xpointsDistance[1].X;
            int rightEdge = xpointsDistance[2].X;

            var valleyPoint = (from vPoints in orderedAsc
                               where vPoints.Value.X > leftEdge && vPoints.Value.X < rightEdge - 100 && vPoints.Value.Y < yEdge
                               orderby vPoints.Key ascending
                               select vPoints.Value).ToList();

            Point firstValley = new Point(valleyPoint[0].X, valleyPoint[0].Y);

            this.imgHandContour.SetPixel(firstValley.X, firstValley.Y, Color.Red); //this line and the bellow one should be on a separated methos to avoid duplicatiom with the other if bellow
            this.graphicsControl.DrawEllipse(new Pen(Color.Red, 2), firstValley.X, firstValley.Y, 10, 10);


            leftEdge = xpointsDistance[2].X;
            rightEdge = xpointsDistance[3].X;

            valleyPoint = (from vPoints in orderedAsc
                           where vPoints.Value.X > leftEdge - 150 && vPoints.Value.X < rightEdge - 100 && vPoints.Value.Y < yEdge
                           orderby vPoints.Key ascending
                           select vPoints.Value).ToList();

            Point secondValley = new Point(valleyPoint[0].X, valleyPoint[0].Y);

            this.imgHandContour.SetPixel(secondValley.X, secondValley.Y, Color.Red); //this line and the bellow one should be on a separated methos to avoid duplicatiom with the other if bellow
            this.graphicsControl.DrawEllipse(new Pen(Color.Red, 2), secondValley.X, secondValley.Y, 10, 10);

            leftEdge = xpointsDistance[3].X;
            rightEdge = xpointsDistance[4].X;

            valleyPoint = (from vPoints in orderedAsc
                           where vPoints.Value.X > leftEdge - 150 && vPoints.Value.X < rightEdge - 100 && vPoints.Value.Y < yEdge
                           orderby vPoints.Key ascending
                           select vPoints.Value).ToList();

            Point thirdValley = new Point(valleyPoint[0].X, valleyPoint[0].Y);

            this.imgHandContour.SetPixel(thirdValley.X, thirdValley.Y, Color.Red); //this line and the bellow one should be on a separated methos to avoid duplicatiom with the other if bellow
            this.graphicsControl.DrawEllipse(new Pen(Color.Red, 2), thirdValley.X, thirdValley.Y, 10, 10);
        }

        private void CalculateNeighborhoodWhitePixelCount()
        {
            foreach (Point point in lstRedPixels)
            {
                int iWhitePixelCount = 0;
                Color cl = this.imgHandContour.GetPixel(point.X, point.Y);

                //white neighbor counting
                if (this.imgHandContour.GetPixel(point.X - 1, point.Y).Name == "ffffffff")
                    iWhitePixelCount++;

                if (this.imgHandContour.GetPixel(point.X + 1, point.Y).Name == "ffffffff")
                    iWhitePixelCount++;

                if (this.imgHandContour.GetPixel(point.X, point.Y - 1).Name == "ffffffff")
                    iWhitePixelCount++;

                if (this.imgHandContour.GetPixel(point.X, point.Y + 1).Name == "ffffffff")
                    iWhitePixelCount++;

                //90% of the width is being processed. the other 10% is not necessary and if included add noise to the hand shape.
                if (iWhitePixelCount > 2 && point.X < 1440)
                    this.imgHandContour.SetPixel(point.X, point.Y, Color.White);

                else
                    lstCurvaturePoints.Add(new Point(point.X, point.Y));
            }            
        }       

        #endregion
    }
}
