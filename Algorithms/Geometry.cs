﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Algorithms
{
    public static class Geometry
    {
        public static double EuclideanDistance(Point p1, Point p2)
        {
            double distance = 0;
            double d1Diff = Math.Pow((p2.X - p1.X), 2);
            double d2Diff = Math.Pow((p2.Y - p1.Y), 2);
            distance = Math.Sqrt(d1Diff + d2Diff);
            return distance;
        }
    }
}
