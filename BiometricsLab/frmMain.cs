﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Image.Pool;

namespace BiometricsLab
{
    public partial class frmMain : Form
    {
        #region Private Methods

        private System.Drawing.Image imgOriginal;

        #endregion

        #region Constructor

        public frmMain()
        {
            InitializeComponent();

            //images for testing:

            //imgOriginal = System.Drawing.Image.FromFile("C:\\029_4.jpg");          //Filtros=OK(de primeira); TOPS = OK; VALLEYS NOK
            //imgOriginal = System.Drawing.Image.FromFile("C:\\018_6.jpg");          //Filtros=OK(de primeira); TOPS = OK; VALLEYS NOK
            //imgOriginal = System.Drawing.Image.FromFile("C:\\007_2.jpg");          //Filtros=OK(de primeira); TOPS = OK; VALLEYS NOK
            //imgOriginal = System.Drawing.Image.FromFile("C:\\024_2.jpg");          //Filtros=OK(de primeira); TOPS = OK; VALLEYS NOK
            //imgOriginal = System.Drawing.Image.FromFile("C:\\020_5.jpg");          //Filtros=Ajustar Threshold;TOPS = OK; VALLEYS NOK
            //imgOriginal = System.Drawing.Image.FromFile("C:\\108_1.jpg");          //Filtros=Ajustar Threshold;TOPS = OK; VALLEYS NOK
            //imgOriginal = System.Drawing.Image.FromFile("C:\\034_2.jpg");            //aumentar X?
            //imgOriginal = System.Drawing.Image.FromFile("C:\\071_4.jpg");          //Filtros=Ajustar Threshold;TOPS = OK; VALLEYS NOK
            //imgOriginal = System.Drawing.Image.FromFile("C:\\152_4.jpg");          //Filtros=Ajustar Threshold;TOPS = OK; VALLEYS NOK
            //imgOriginal = System.Drawing.Image.FromFile("C:\\209_4.jpg");          //Filtros=Ajustar Threshold;TOPS = OK; VALLEYS NOK

            //from CASIA
            imgOriginal = System.Drawing.Image.FromFile("C:\\001_l_940_06.jpg");
            //imgOriginal = System.Drawing.Image.FromFile("C:\\042_l_940_03.jpg");
           
            
        }

        #endregion

        #region Private Events

        private void frmMain_Load(object sender, EventArgs e)
        {     
            //rotates the image if it's not on standard portrait visualization
            if (Array.IndexOf(imgOriginal.PropertyIdList, 274) > -1)
            {
                var orientation = (int)imgOriginal.GetPropertyItem(274).Value[0];
                if (orientation == 6)
                    imgOriginal.RotateFlip(RotateFlipType.Rotate90FlipNone);              
            }
            
            this.pictureBoxMain.Image = imgOriginal;
            Pool.Instance.Original = new Bitmap(imgOriginal);
            this.KeyPreview = true;
        }

        private void btnBinarization_Click(object sender, EventArgs e)
        {
            frmThreshold frmThres = new frmThreshold(this.imgOriginal);
            frmThres.Show();           
        }

        private void btnEdgeDetection_Click(object sender, EventArgs e)
        {
            /**TODO: 1 - place this and other checks at GUIValidation
             *       2 - Replace the strings for variables (for later localisation)
             */
            if (Pool.Instance.Binary == null)
            {
                MessageBox.Show("You must binarize the image first!","Invalid Operation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmEdgeDetection frmED = new frmEdgeDetection(Pool.Instance.Binary);
            frmED.Show();
        }

        private void btnHandContour_Click(object sender, EventArgs e)
        {
            /**TODO: 1 - place this and other checks at GUIValidation
             *       2 - Replace the strings for variables (for later localisation)
             */
            if (Pool.Instance.EdgeDetected == null)
            {
                MessageBox.Show("You must binarize and detect the edges of the image first!", "Invalid Operation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            frmHandContour frmHC = new frmHandContour(Pool.Instance.EdgeDetected);
            frmHC.Show();
        }

        private void pictureBoxMain_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "*.bmp|";
            ofd.InitialDirectory = @"C:\";
            ofd.Title = "Please select an bitmap image file:";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.imgOriginal = System.Drawing.Image.FromFile(ofd.FileName);
                this.pictureBoxMain.Image = this.imgOriginal;
                Pool.Instance.Original = new Bitmap(this.imgOriginal);
                this.pictureBoxMain.Refresh();                   
            }
        }

        /**
         * Apply all filter operations
         * TODO: Separate this on threads
         */
        private void btnApplyAll_Click(object sender, EventArgs e)
        {
            frmThreshold frmThres = new frmThreshold(this.imgOriginal);
            frmThres.Show();
            frmThres.Hide();
            frmEdgeDetection frmED = new frmEdgeDetection(Pool.Instance.Binary);
            frmED.Show();
            frmED.Hide();
            frmHandContour frmHC = new frmHandContour(Pool.Instance.EdgeDetected);
            frmHC.Show();
        }

     
        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A)
                btnApplyAll_Click(sender, e);                
            
            //Opens the Binarization form.
            if (e.KeyCode == Keys.B)
                btnBinarization_Click(sender, e);    
        }

        private void binarizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnBinarization_Click(sender, e);
        }

        private void edgeDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnEdgeDetection_Click(sender, e);
        }

        private void extractHandContourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnHandContour_Click(sender, e);
        }

        private void applyAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnApplyAll_Click(sender, e);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout frmA = new frmAbout();
            frmA.Show();               
        }        

        //TODO: created a method for set original image.

        #endregion
    }
}
